//
// Object destructuring
//

// const book = {
//   title: 'Ego is the Enemy',
//   author: 'Ryan Holiday',
//   publisher: {
//     //name: 'Penguin'
//   }
// }
//
// const { name: publisherName = 'Self-Published'  } = book.publisher;
//
// console.log(publisherName);

//
// Array destructuring
//

const item = ['Coffee (hot)', 'R10', 'R12.50', 'R12.75'];

const [product, , mediumPrice] = item;

console.log(`A medium ${product} cost ${mediumPrice}`);
