import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyBeTu6PJgzoQ_rFhU0C3O5xlR8yyjypDiY",
  authDomain: "expensify-app-48b5f.firebaseapp.com",
  databaseURL: "https://expensify-app-48b5f.firebaseio.com",
  projectId: "expensify-app-48b5f",
  storageBucket: "expensify-app-48b5f.appspot.com",
  messagingSenderId: "661310924757"
};

firebase.initializeApp(config);

const database = firebase.database();

// // child_removed
// database.ref('expenses').on('child_removed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val());
// });
//
// // child_changed
// database.ref('expenses').on('child_changed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val());
// });
//
// //child_added
// database.ref('expenses').on('child_added', (snapshot) => {
//   console.log(snapshot.key, snapshot.val());
// });
//
// // database.ref('expenses')
// //   .on('value', (snapshot) => {
// //     const expenses = [];
// //
// //     snapshot.forEach((childSnapshot) => {
// //       expenses.push({
// //         id: childSnapshot.key,
// //         ...childSnapshot.val()
// //       });
// //     });
// //     console.log(expenses);
// //   });
//
//
// database.ref('expenses').push({
//   description: 'Rent',
//   note: 'Last month rent',
//   amount: 55000,
//   createdAt: 1000
// });
//
// // database.ref().on('value', (snapshot) => {
// //   const val = snapshot.val();
// //   console.log(`${val.name} is a ${val.job.title} at ${val.job.company}`);
// // }, (e) => {
// //   console.log('Error with fetaching the data', e);
// // });
//
// // database.ref().set({
// //   name: 'Jeremie Negie',
// //   age: 21,
// //   stressLevel: 4,
// //   job: {
// //     title: 'Software developer',
// //     company: 'Google'
// //   },
// //   location: {
// //     city: 'Johannesburg',
// //     country: 'South Africa',
// //   },
// //   attributes: {
// //     height: '174m',
// //     weight: '80kg'
// //   }
// // }).then(() => {
// //   console.log('Data is saved');
// // }).catch((e) => {
// //   console.log('Something went wrong', e);
// // });
// //
// // database.ref().update({
// //   stressLevel: 9,
// //   'job/company': 'Amazon',
// //   'location/city': 'Seattle',
// //   'location/country': 'United State'
// //
// // }).then(() => {
// //   console.log('Data has been updated');
// // }).catch(() => {
// //   console.log('Something went south', error);
// // })
//
// // database.ref('User/isSingl').remove()
// //   .then(() => {
// //     console.log('Remove successfuly')
// //   })
// //   .catch((error) => {
// //     console.log('Something went wrong', error);
// //   });