export default (expenses) => expenses
  .map((expense) => expense.amount)
  .reduce((acc, currVal) => currVal + acc, 0);