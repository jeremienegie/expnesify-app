import selectExpenseTotal from '../../selectors/expenses-total';
import expenses from '../fixtures/expenses';

test('should return the some of all expenses', () => {
  const total = selectExpenseTotal(expenses);
  expect(total).toBe(114195); //114195
});

test('should return the some of one expense', () => {
  const total = selectExpenseTotal([expenses[0]]);
  expect(total).toBe(195);
});

test('should return 0 if no expenses', () => {
  const total = selectExpenseTotal([]);
  expect(total).toBe(0);
});