import moment from 'moment';
import filtersReducr from '../../reducers/filters'

test('should setup default filters', () => {
  const state = filtersReducr(undefined, { type: '@@INIT' });

  expect(state).toEqual({
    text: '',
    sortBy: 'date',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
  });
});

test('should set sortBy amount', () => {
  const state = filtersReducr(undefined, { type: 'SORT_BY_AMOUNT' });

  expect(state.sortBy).toBe('amount');
});

test('should set sortBy date', () => {
  const state = filtersReducr({
    text: '',
    sortBy: 'amount',
    startDate: undefined,
    endDate: undefined
  }, { type: 'SORT_BY_DATE' });

  expect(state.sortBy).toBe('date');
});

test('should set text filter', () => {
  const text = 'hello';
  const action = {
    type: 'SET_TEXT_FILTER',
    text
  }
  const state = filtersReducr(undefined, action);

  expect(state.text).toBe(text);
});

test('should set startDate', () => {
  const startDate = moment();
  const action = {
    type: 'SET_START_DATE',
    startDate
  }
  const state = filtersReducr(undefined, action);

  expect(state.startDate).toEqual(startDate);
});

test('should set endDate', () => {
  const endDate = moment();
  const action = {
    type: 'SET_END_DATE',
    endDate
  }
  const state = filtersReducr(undefined, action);

  expect(state.endDate).toEqual(endDate);
});