import React from 'react';

import authReducers from '../../reducers/auth';

test('should set default state correctly', () => {
  const state = authReducers(undefined, { type: '@@INIT' })

  expect(state).toEqual({});
});

test('should set uid for login', () => {
  const uid = '123abc';
  const action = {
    type: 'LOGIN',
    uid
  };
  const state = authReducers({}, action);
  expect(state.uid).toBe(action.uid)
});

test('should clear uid for logout', () => {
  const action = {
    type: 'LOGOUT'
  }
  const state = authReducers({ uid: '123abc' }, action);
  expect(state).toEqual({});
});